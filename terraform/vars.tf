output "public_ip" {
	value = openstack_networking_floatingip_v2.fip_tf.address
}

variable "domain" {
	default = "opusdv.online"
}

variable "subdomain" {
	default = "vpn"
}
