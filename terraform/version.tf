terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
		}
		selectel = {
			source  = "selectel/selectel"
			version = "3.8.0"
		}
		vault = {
			source = "hashicorp/vault"
			version = "3.4.1"
		}
	}
}
