provider "vault" {
	address = "https://vault-cluster.vault.f29a2d06-7f3a-4476-a02b-220fe7deef2e.aws.hashicorp.cloud:8200"
	skip_tls_verify = true
	token = "hvs.CAESIASALflbudCg7OVA-qDDjBFC7jiWwzs8nPUpc3N962XBGicKImh2cy5EcjliY1pidTQ0cmoxRTB5TWJpMjE4aDAucnJoUlMQ2Ac"
}


provider "openstack" {
  auth_url    = data.vault_generic_secret.vpn.data["auth_url"]
	domain_name = data.vault_generic_secret.vpn.data["domain_name"]
  tenant_id		= data.vault_generic_secret.vpn.data["tenant_id"]
	user_name   = data.vault_generic_secret.vpn.data["user_name"]
  password    = data.vault_generic_secret.vpn.data["password"]
  region      = data.vault_generic_secret.vpn.data["region"]
}

provider "selectel" {
	token = data.vault_generic_secret.vpn.data["selectel_api_key"]
}

